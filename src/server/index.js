// load the variables!
require('./config/config');

const express = require('express');
const os = require('os');

const _ = require('lodash');
const {ObjectID} = require('mongodb');
const bcrypt = require('bcryptjs')
// use this so you can pass JSON in the request body
const bodyParser = require('body-parser');

const { mongoose } = require('./db/mongoose');

const { Todo } = require('./models/todo');
const { User } = require('./models/user');
const { authenticate } = require('./middleware/authenticate');


const app = express();

// express middleware, you can configure it. 
// look it up
// used for parsing http request bodies
app.use(bodyParser.json());
// used for serving static content
app.use(express.static('dist'));





app.get('/todos', authenticate, (req, res) => {
    Todo.find({
        _creator: req.user._id
    }).then((todos) => {
        res.send({todos});
    }, (err) => {
        res.status(400).send(err);
    });
});

app.post('/todos', authenticate, (req, res) => {
    const todo = new Todo({
        text: req.body.text,
        _creator: req.user._id
    });

    todo.save().then((doc) => {
        res.send(doc); 
    }, (err) => {
        res.status(400).send(err);
    });
});

app.get('/todos/:id', authenticate, (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id))
        return res.status(404).send();

    Todo.findById(id).then((todo) => {
        if (!todo)
            return res.status(404).send();

        if (todo._creator.toHexString() != req.user._id)
            return Promise.reject();

        res.send({todo});
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.delete('/todos/:id', authenticate, (req, res) => {
    const id = req.params.id;
    
    if (!ObjectID.isValid(id))
        res.status(400).send();
    
    Todo.findOneAndRemove({
        _id: new ObjectID(id),
        _creator: req.user._id
    }).then((todo) => {
        if (!todo)
            return res.status(404).send();

        res.send({todo});
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.patch('/todos/:id', authenticate, (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id))
        return res.status(400).send();
    
    let body = _.pick(req.body, ['text', 'completed']); 

    if (_.isBoolean(body.completed) && _.isString(body.text)) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findOneAndUpdate({
        _id: new ObjectID(id),
        _creator: req.user._id
    }, { $set: body }, { new: true })
    .then((todo) => {
        if (!todo)
            return res.status(404).send();

        return res.send({todo});    
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.post('/users', (req, res) => {
    const body = _.pick(req.body, ['email', 'password']);
    const user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token).send({user});
    }).catch((e) => res.status(400).send(e));
    // // alternatively
    // try {
    //     await user.save();
    //     const token = await user.generateAuthToken();
    //     res.header('x-auth', token).send({user});
    // } catch (e) {
    // // WARNING: Don't send the stack trace! Ever!
    //   res.status(400).send(e);
    //}

});

app.post('/users/login', (req, res) => {
    const body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        return user.generateAuthToken().then((token) => {
            res.header('x-auth', token).send({user});
        });
    }).catch((e) => res.status(400).send());
});

app.delete('/users/me/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
        res.send();
    }, () =>{
        res.status(400).send();
    });
});

app.get('/users/me', authenticate, (req, res) => {
    res.send(req.user);
});

app.get('/api/getUsername', (req, res) => res.send({ username: os.userInfo().username }));
app.post('/api/something', async (req, res) => {
    res.sendStatus(200);
    console.log('this works');
});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
