import React, { Component } from 'react';
import './app.css';
import ReactImage from './react.png';
import axios from 'axios';

export default class App extends Component {
  state = { username: null };

  componentDidMount() {
    fetch('/api/getUsername')
      .then(res => res.json())
      .then(user => this.setState({ username: user.username }));
  }

  dugmeClicked() {
    axios.post('/api/something', {}).then(res => 
      console.log(res)
    )
  }

  render() {
    const { username } = this.state;
    return (
      <div>
        {username ? <h1>{`Hello ${username}`}</h1> : <h1>Loading.. please wait!</h1>}
        <img src={ReactImage} alt="react" />
        <button onClick={this.dugmeClicked}>DUGME</button>
      </div>
    );
  }
}
